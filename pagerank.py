import json
import scipy.linalg as lalg
import scipy


class PageRank:

    def __init__(self, whole_links, alpha=0.2):
        self.whole_links = whole_links
        self.alpha = alpha
        self.size = len(self.whole_links)
        self.map_ids = [0] * self.size
        self.map()
        self.v = [1/self.size] * self.size
        self.p_matrix = self.calc_p_matrix()



    def map(self):
        for i, doc_id in enumerate(self.whole_links):
            self.map_ids[i] = doc_id


    def calc_p_matrix(self):
        n = self.size
        mat = []

        for i in range(n):
            doc_id = self.map_ids[i]
            links = self.whole_links[doc_id]
            row = [(1/len(links) if self.map_ids[l] in links else 0) for l in range(n)]
            row = [tup[0] * (1 - self.alpha) + \
                   tup[1] * self.alpha
                   for tup in zip(row, self.v)]
            mat.append(row)
        return mat


    def calc_rank(self):
        mat = lalg.eig(self.p_matrix, left=True, right=False)[1][:,0]
        self.rank = (mat/sum(mat)).real
        dic = {}
        for i in range(self.size):
            dic[self.map_ids[i]] = self.rank[i]
        return dic

# a = {'0':{'0', '1', '2', '3'},
#      '1':{'0', '1', '2'},
#      '2':{'0', '1'},
#      '3':{'0'},
#      '4':{}}
#
# # a = [[1, 1, 1, 1, 0],
# #          [1, 1, 1, 0, 0],
# #          [1, 1, 0, 0, 0],
# #          [1, 0, 0, 0, 0],
# #          [0, 0, 0, 0, 0]]
# print(PageRank(a, 0.1).calc_rank())
