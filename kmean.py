import math
import random
from operator import itemgetter


class Point:
    def __init__(self, id, vector):
        self.id = id
        self.vector = vector

class Cluster:

    def __init__(self, points):
        self.label = ''
        self.points = points
        self.centroid = self.calculate_centroid()

    def __str__(self):
        return ('-'*10 + 'Cluster label: ' + self.label + '\nDocs:\n' + ', '.join([p.id for p in self.points])+ '\n'+ '-'*10)

    def update(self, points):
        old_centroid = self.centroid
        self.points = points
        self.centroid = self.calculate_centroid()
        return get_distance(old_centroid, self.centroid)

    def calculate_centroid(self):
        vectors = [p.vector for p in self.points]
        return [math.fsum(l) / len(self.points) for l in zip(*vectors)]



# ===================== Clustering =================== #
def count_point_has_term(points, term_index):
    s = 0
    for p in points:
        if p.vector[term_index] != 0:
            s += 1
    return s

def get_distance(vector1, vector2):
    d_2 = sum([pow(vector1[i] - vector2[i], 2) for i in range(len(vector1))])
    return math.sqrt(d_2)


def get_cost(clusters):
    return sum([sum([get_distance(p.vector, c.centroid) for p in c.points]) for c in clusters])


def k_means_seed(points, k, t_hold, seed):
    """
        Find clusters of points, k = number of clusters, t_hold = threshold of convergence
    """
    init_centroids = seed              # random points for initial centroids

    clusters = [Cluster([v]) for v in init_centroids]

    loop_counter = 0
    while True:
        clusters_points = [[] for c in clusters]
        loop_counter += 1

        for p in points:
            # For calculating min_distance set the first one
            min_distance, cluster_index = get_distance(p.vector, clusters[0].centroid), 0

            for i in range(1, k):
                new_distance = get_distance(p.vector, clusters[i].centroid)
                if min_distance > new_distance:
                    min_distance, cluster_index = new_distance, i

            clusters_points[cluster_index].append(p)

        # Calculating convergence factor
        max_shift = 0.0
        for i in range(k):
            new_shift = clusters[i].update(clusters_points[i])
            max_shift = max(max_shift, new_shift)

        # If the centroids have stopped moving much, say we're done!
        if max_shift < t_hold:
            print("Converged after %s iterations" % loop_counter)
            break
    return clusters


def k_means(points, k, t_hold, tries = 3, landa = 6):
    seed = random.sample(points, k)
    clusters = k_means_seed(points, k, t_hold, seed)
    cost = get_cost(clusters)

    for i in range(tries-1):
        seed = random.sample(points, k)
        new_clusters = k_means_seed(points, k, t_hold, seed)
        new_cost = get_cost(clusters)
        if new_cost < cost:
            cost, clusters = new_cost, new_clusters

    return clusters, (cost + landa * k)

def clustering(points, L=-1, conv_thold = 0.02, num_thold = 0.1):
    max_num = L if L != -1 else len(points)
    if L == 0:
        raise Exception("ERROR: Dude, L = 0 ???")

    min_clusters, min_cost = k_means(points, 1, conv_thold)
    num_of_clusters = 1

    for k in range(2, max_num + 1):
        new_clusters, new_cost = k_means(points, k, conv_thold)

        if new_cost < min_cost : #and min_cost - new_cost > num_thold:
            min_clusters, min_cost = new_clusters, new_cost
            num_of_clusters = k
            # else:
            #     break

    return {
        'k': num_of_clusters,
        'clusters': min_clusters,
        'cost': min_cost

    }




# ========================== Labeling MOFO ==================== #
def term_score (term_index, cluster, N__, N_1, N_0, whole_points):
    N1_ = count_point_has_term(whole_points, term_index)
    N0_ = N__ - N1_
    N11 = count_point_has_term(cluster.points, term_index)
    N01 = N_1 - N11
    N10 = N1_ - N11
    N00 = N_0 - N10

    calc = ((N11) / (N__) * math.log2((N__) * (N11) / (N1_) / (N_1)) if N11 != 0 else 0)+ \
           ((N01) / (N__) * math.log2((N__) * (N01) / (N0_) / (N_1)) if N01 != 0 else 0)+ \
           ((N10) / (N__) * math.log2((N__) * (N10) / (N1_) / (N_0)) if N10 != 0 else 0)+ \
           ((N00) / (N__) * math.log2((N__) * (N00) / (N0_) / (N_0)) if N00 != 0 else 0)
    return calc

def find_label(cluster, N__, vocabs, whole_points):
    labels = [{'term_index': -1, 'term_score': 0},
              {'term_index': -1, 'term_score': 0},
              {'term_index': -1, 'term_score': 0},
              {'term_index': -1, 'term_score': 0},
              {'term_index': -1, 'term_score': 0}]

    N_1 = len(cluster.points)
    N_0 = N__ - N_1

    for ti in range(len(vocabs)):
        ts = term_score(ti, cluster, N__, N_1, N_0, whole_points)
        if ts > float(labels[-1]['term_score']):
            labels[-1]['term_score'] = ts
            labels[-1]['term_index'] = ti
            labels = sorted(labels, key=itemgetter('term_score'), reverse=True)

    ret = [i['term_index'] for i in labels]
    label = []
    for ti in ret:
        if ti > -1:
            label.append(vocabs[ti])
    cluster.label = ' '.join(label)
    return cluster





# ================== input conversion ================== #
def get_points(doc_dict, vocabs):
    """
        { doc_id : { term : tf}
        vocabs = [...]
    """
    points = []
    for doc_id in doc_dict:
        vector = []
        for v in vocabs:
            tf = doc_dict[doc_id][v] if v in doc_dict[doc_id] else 0
            vector.append(tf)
        points.append(Point(doc_id, vector))

    return points




# ======================= main ===================== #
def main(doc_dict, vocabs):
    whole_points = get_points(doc_dict, vocabs)
    cl_infos = clustering(whole_points)

    N__ = len(whole_points)

    # cl_infos['labels'] \
    q= [find_label(cls, N__, vocabs, whole_points) for cls in cl_infos['clusters']]
    print(cl_infos)
    return cl_infos
    # for c in range(len(cl_infos['labels'])):
    #     labels = []
    #     for ti in cl_infos['labels'][c]:
    #         if ti > -1:
    #             labels.append(vocabs[ti])
    #     cl_infos['labels'][c] = labels
    # print(cl_infos['labels'])



###########################################################################################
# docs = {'doc1': { 'a': 3,
#                   'b': 1},
#
#         'doc2': { 'd': 4,
#                   'b': 5},
#
#         'doc3': { 'a': 1,
#                   'g': 4,
#                   'd': 8}
#         }
#
# vocabs= ['a','b','d','g']
#
# main(docs, vocabs)