# -*- coding: utf-8 -*-
import os

import scrapy, bs4, re, json
from bs4 import BeautifulSoup
from scrapy.crawler import CrawlerProcess
from scrapy.exceptions import CloseSpider
from scrapy.utils.project import get_project_settings
from hazm import Normalizer

from util import printProgressBar

DOC_FOLDER = os.getcwd() + '/docs/'
MAX_CRAWL_NUM = 80
OUT_DEGREE = 10
BASE_URL = 'https://fa.wikipedia.org'

class WikiSpiderSpider(scrapy.Spider):
    name = "wiki_spider"
    allowed_domains = ["fa.wikipedia.org"]
    # start_urls = ["https://fa.wikipedia.org/wiki/%D8%B3%D8%B9%D8%AF%DB%8C"]

    crawled_urls = []
    crawled_num = 0

    custom_settings = {
        # 'LOG_ENABLED': False,
        'DEPTH_PRIORITY': 1,
        'SCHEDULER_DISK_QUEUE': 'scrapy.squeues.PickleFifoDiskQueue',
        'SCHEDULER_MEMORY_QUEUE': 'scrapy.squeues.FifoMemoryQueue',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
    }

    def __init__(self, *args, **kwargs):
        super(WikiSpiderSpider, self).__init__(*args, **kwargs)
        self.settings = {'LOG_LEVEL': "ERROR"}
        self.start_urls = kwargs.get('start_urls')
        self.out_degree = kwargs['out_degree']
        self.max_crawl_num = kwargs['max_crawl_num']
        printProgressBar(0, self.max_crawl_num, prefix='Crawling Progress:', suffix='Complete', length=50)

    def link_check(self, link):
        # anchor check
        anchor = link.get_text()
        if anchor == '':
            return None
        for symb in [r'\[', r'\]', r'\d', r'[۱-۹]', r'\:']:
            if re.search(symb, anchor):
                return None

        # href check
        href = link['href']
        if not href.startswith('/wiki/'):
            return None
        for symb in [':', '.']:
            if symb in href:
                return None
        if '#' in href:
            if self.url == BASE_URL + href[:href.find('#')]:
                return None
            else:
                return (anchor, BASE_URL + BASE_URL + href[:href.find('#')])

        return (anchor, BASE_URL + href)

    def extract_title(self):
        return self.soup.find('h1', {'class': "firstHeading"}).get_text().strip()

    def extract_abstract(self):
        return (self.crawl_info.find(id='mw-content-text',
                                     recursive=False)).find('p', recursive=False).get_text().strip()
    def extract_infobox(self):
        infobox = self.crawl_info.find(class_='infobox')
        if infobox:
            infobox = infobox.get_text().strip()
            while('\n\n' in infobox):
                infobox = infobox.replace('\n\n', '\n')
            return infobox
        else:
            return ''

    def decompose_extra(self):
        crawl_info = self.crawl_info
        # remove comments
        comments = crawl_info.find_all(string=lambda text: isinstance(text, bs4.Comment))
        for comment in comments:
            comment.extract()

        # removable tags
        remove_list = list(set(crawl_info.find_all(['sup', 'script', 'noscript']) + \
                               crawl_info.select('span.mw-editsection') + \
                               crawl_info.find_all(class_='plainlinks') + \
                               crawl_info.select('div.toc') + \
                               crawl_info.select('table.infobox') + \
                               crawl_info.select('div.dablink') + \
                               crawl_info.select('div.hatnote.relarticle.mainarticle') + \
                               crawl_info.select('div.reflist') + \
                               crawl_info.select('table.navbox') + \
                               crawl_info.select('div.printfooter') + \
                               crawl_info.select('div.visualClear') + \
                               crawl_info.select('div.mw-jump') + \
                               crawl_info.select('div.mw-hidden-catlinks') + \
                               crawl_info.find_all(id='siteSub') + \
                               crawl_info.find_all(id='contentSub')
                               ))

        for item in remove_list:
            item.decompose()
        return crawl_info

    ############################################################
    ############################################################
    def parse(self, response):

        # check for ending
        if self.crawled_num >= self.max_crawl_num:
            raise CloseSpider(str(self.max_crawl_num) + ' Page has Crawled')

        # add NEW page
        # print('PROGRESS: ', self.crawled_num)
        url = response.url
        self.crawled_urls.append(url)
        self.crawled_num += 1
        id = self.crawled_num
        # print('p1 ' + str(id))

        # soup parser initial
        self.soup = BeautifulSoup(response.text, 'html.parser')

        # # extract Title
        title = self.extract_title()
        # print(title)
        # print('p2 ' + str(id) + ' ' + title)

        self.crawl_info = self.soup.find('div', {'id': "bodyContent"})
        infobox = self.extract_infobox()

        self.crawl_info = self.decompose_extra()  # "mw-content-text"})

        # # extract Abstract
        abstract = self.extract_abstract()
        # print(abstract)
        # print('p3 ' + str(id))

        # # extract content
        content = self.crawl_info.get_text().replace('\n', ' ').strip()
        # print(content)
        # print('p4 ' + str(id))

        # extract out links
        new_out_links = []
        j = 0
        for link in self.crawl_info.find_all('a'):
            tup = self.link_check(link)
            fl = False
            if tup:
                for already in new_out_links:
                    if tup[1] == already['url']:
                        fl = True
                        break
                if not fl:
                    new_out_links.append({'anchor': tup[0], 'url': tup[1]})
                # print(tup[1])

        # print('p5 ' + str(id))

        doc = {
            'url': url,
            'title': title,
            'abstract': abstract,
            'content': content,
            # 'infobox': infobox,
            'links': new_out_links
        }
        with open(DOC_FOLDER + str(id) + '.json', 'w') as f:
            json.dump(doc, f)
        # print('p6 ' + str(id))

        # print(self.crawled_num)
        printProgressBar(self.crawled_num, self.max_crawl_num, prefix='Crawling Progress:', suffix='Complete', length=50)

        for l in new_out_links[:OUT_DEGREE]:
            yield scrapy.Request(l['url'], callback=self.parse)


def run_crawler(start_urls=['https://fa.wikipedia.org/wiki/%D8%AD%D8%A7%D9%81%D8%B8'], out_degree=10, max_crawl_num=80):
    crawler_path = '/home/arash/Projects/PycharmProjects/Wiki/wiki_crawler/'
    os.chdir(crawler_path)
    settings = get_project_settings()
    process = CrawlerProcess(settings)
    process.crawl('wiki_spider', start_urls=start_urls, out_degree=out_degree, max_crawl_num=max_crawl_num)
    try:
        process.start()
    except CloseSpider:
        pass