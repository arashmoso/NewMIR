from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from elasticsrch import ElasticIndex
from wiki_crawler.spiders.wiki_spider import run_crawler
import os


# os.system("rm -rf docs && mkdir docs")
# run_crawler(out_degree=1, max_crawl_num=10)

def main():
    es = ElasticIndex()
    n = 99
    while (True):
        n = int(input(""" select:
                  1- crawl webpages
                  2- index all pages
                  3- cluster pages
                  4- calculate pagerank
                  5- search
                  6- others
                  0- exit
                  """))
        if n == 1:
            start_url = input('enter your start url:')
            out_degree = int(input('enter your out degree:'))
            max_crawl = int(input('enter your max crawl number:'))
            print('crawling started')
            os.system("rm -rf docs && mkdir docs")
            run_crawler(out_degree=3, max_crawl_num=20)
            print('\ncrawling finished\n')
        elif n == 2:
            es.index_all()
        elif n == 3:
            es.make_clusters()
        elif n == 4:
            es.calc_pagerank()
        elif n == 5:
            query = input("Query? :")
            field_weights = {'title': int(input("weight for title:")), 'abstract': int(input("weight for abstract:")),
                             'content': int(input("weight for content:"))}
            print("clusters: ")
            print(es.cluster_labels)
            cluster = int(input("cluster id (-1 for not-clustered search):"))
            pr = input("pagerank? (True/Flase):")
            pagerank = True if pr == "True" else False
            print(es.search(query=query, field_weights=field_weights, cluster=cluster, pageRank=pagerank))
        elif n == 6:
            n = int(input(""" Others:
                          1- print doc dict
                          2- print vocab
                          3- clear indices
                          4- load vocab from file"""))
            if n == 1:
                print(es.doc_index_map)
            elif n == 2:
                print(es.VOCAB)
            elif n == 3:
                es.clear_indices()
            elif n == 4:
                es.loadVacabulary()
        else:
            break

main()