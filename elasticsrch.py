import json
import os
from elasticsearch import Elasticsearch
import kmean
import pagerank


class ElasticIndex:
	def __init__(self):
		self.ES_HOST = {"host": "localhost", "port": 9200}
		self.INDEX_NAME = 'wikipedia_fa'
		self.DOC_TYPE = 'doc'
		self.DOC_GRAPH_TYPE = "graph"
		self.DOC_CLUSTER_TYPE = "cluster"
		self.es = Elasticsearch(hosts=[self.ES_HOST])
		self.doc_index_map = dict()
		self.VOCAB = dict()
		self.TERM_INDEX = 0
		self.cluster_labels = dict()

	####################################INDEXING################################
	############################################################################

	# delete previous indexes
	def clear_indices(self):
		try:
			self.es.indices.delete(index=self.INDEX_NAME, ignore=[400, 404])
		except Exception as e:
			pass

	def init_index(self):

		self.clear_indices()

		self.es.indices.create(index=self.INDEX_NAME, ignore=400)

		# doc structure
		press_mapping = {self.DOC_TYPE: {"properties": {
			"url": {
				"type": "string"
			},
			'title': {
				'type': 'keyword',
				'index': 'analyzed',
			},
			'abstract': {
				'type': 'text',
				'term_vector': 'yes',
				'index': 'analyzed',
			},
			'content': {
				'type': 'text',
				'index': 'analyzed',
				'term_vector': 'yes',
			},
			'links': {
				'type': 'object'
			}
		}}}

		# apply the mapping
		self.es.indices.put_mapping(
			index=self.INDEX_NAME,
			doc_type=self.DOC_TYPE,
			body=press_mapping
		)

	# index all docs in the /docs directory
	def index_all(self):
		self.init_index()
		doc_index = 0
		path = os.getcwd() + "/docs"
		for filename in os.listdir(path):
			if filename.endswith('.json'):
				with open(path + "/" + filename) as open_file:
					jdoc = json.load(open_file)
					TEMP_DOC = {'url': jdoc['url'], 'title': jdoc['title'], 'abstract': jdoc[
						'abstract'], 'content': jdoc['content'], 'links': jdoc['links'], 'cluster': '-1', 'pagerank': 0}
					self.es.index(index=self.INDEX_NAME, doc_type=self.DOC_TYPE,
					              body=TEMP_DOC, id=doc_index)
					self.doc_index_map.update({jdoc['title']: doc_index})
					print(doc_index)
					doc_index += 1
		self.saveVocabulary()

	############################################################################
	############################################################################

	# return term vector for a doc
	def getTermVector(self, doc_id):
		temp = dict()
		a = self.es.termvectors(index=self.INDEX_NAME,
		                        doc_type=self.DOC_TYPE,
		                        id=doc_id,
		                        field_statistics=True,
		                        fields=['abstract', 'content'],
		                        term_statistics=True
		                        )
		curr_content_termvec = a["term_vectors"]["content"]["terms"]
		curr_abst_termvec = a["term_vectors"]["abstract"]["terms"]
		tokens = curr_content_termvec.keys()
		[temp.update({token: curr_content_termvec[token]["term_freq"]})
		 for token in tokens]
		tokens = curr_abst_termvec.keys()
		for token in tokens:
			temp[token] = int(temp.get(token, '0')) + \
			              int(curr_abst_termvec[token]["term_freq"])
		return a["_id"], temp

	# calculate term vector for all docs by scrolling through the index
	def scrollIndex(self):
		ALL_QUERY = {"query": {"match_all": {}}}

		rs = self.es.search(
			index=self.INDEX_NAME,
			scroll='60s',
			size=10,
			body=ALL_QUERY
		)

		scroll_size = rs['hits']['total']
		temp = dict()
		while scroll_size:
			try:
				scroll_id = rs['_scroll_id']
				rs = self.es.scroll(scroll_id=scroll_id, scroll='60s')
				data = rs['hits']['hits']
				for doc in data:
					curr_doc, curr_term_vector = self.getTermVector(doc["_id"])
					temp[curr_doc] = curr_term_vector

				scroll_size = len(rs['hits']['hits'])
			except Exception as e:
				print = (e)
		return temp

	# update the vocab dictionary from the given term vector
	def getVocabulary(self, term_vector):
		for term in term_vector:
			if term not in self.VOCAB.keys():
				self.VOCAB.update({term: self.TERM_INDEX})
				self.TERM_INDEX += 1

	# extract the collection vocab and save it to a file
	def saveVocabulary(self):
		for _, vector in self.scrollIndex().items():
			self.getVocabulary(vector.keys())
		with open('vocab.json', 'w') as f:
			json.dump(self.VOCAB, f)

	# load vocab from file
	def loadVacabulary(self):
		with open('vocab.json', 'r') as f:
			try:
				self.VOCAB = json.load(f)
				return True
			except Exception:
				return False

	# scluster the collection
	def make_clusters(self):
		dic = {}
		for id, vector in self.scrollIndex().items():
			dic[id] = vector
		info = kmean.main(dic, list(self.VOCAB.keys()))
		# print(info)
		clusters, labels = {}, {}
		for i in range(len(info['clusters'])):
			clusters[str(i)] = [p.id for p in info['clusters'][i].points]
			labels[str(i)] = info['clusters'][i].label
		# print(labels)
		# print(clusters)
		self.cluster_labels = labels
		self.set_doc_clusters(clusters, labels)

	def calc_pagerank(self):
		graph = dict()
		for i in range(len(self.doc_index_map)):
			doc = self.es.search(index=self.INDEX_NAME,
			                     doc_type=self.DOC_TYPE,
			                     body={"query": {
				                     "match":{
					                     "_id": str(i)
				                     }
			                        }
			                     })
			for urltoken in doc["hits"]["hits"][0]["_source"]["links"]:
				url = urltoken["url"]
				rs = self.es.search(index=self.INDEX_NAME,
				                    doc_type=self.DOC_TYPE,
				                    body={"query": {"match" :{"url": url}}})
				if len(rs["hits"]["hits"]) > 0:
					id = rs["hits"]["hits"][0]["_id"]
				graph[str(i)]=graph.get(str(i),[]) + [str(id)]

		self.set_doc_pageranks(pagerank.PageRank(
			whole_links=graph).calc_rank())

	# set the cluster field of every doc
	def set_doc_clusters(self, clusters, labels):
		body = []
		for i in clusters:
			for j in clusters[i]:
				self.es.update(index=self.INDEX_NAME,doc_type=self.DOC_TYPE,id=j,body={"doc":{"cluster" : i}})
			obj = dict()
			obj['cluster'] = clusters[i]
			obj['label'] = labels[i]
			self.es.index(index=self.INDEX_NAME,
			              doc_type=self.DOC_CLUSTER_TYPE, id=i, body=obj)
		# print(body)


		# for i in range(len(labels)):
		#     for j in range(len(clusters[i])):
		#         doc_id=clusters[i][j]
		#         obj={
		#             '_op_type': 'update',
		#             '_index': self.INDEX_NAME,
		#             '_type': self.DOC_TYPE,
		#             '_id': doc_id,
		#             'cluster': i
		#         }
		#         body += [obj]
		#     obj = dict([])
		#     obj['cluster'] = clusters[i]
		#     obj['label'] = labels[i]
		#     self.es.index(index=self.INDEX_NAME,
		#                   doc_type=self.DOC_CLUSTER_TYPE, id=i, body=obj)
		# self.es.bulk(index=self.INDEX_NAME,
		#              doc_type=self.DOC_TYPE, body=body)

	# set the pagerank field of every doc
	def set_doc_pageranks(self, pageranks):
		body = []
		for i in pageranks.keys():
			self.es.update(index=self.INDEX_NAME, doc_type=self.DOC_TYPE, id=i, body={"doc": {"pagerank": pageranks[i]}})


	def search(self, query, field_weights={'title': 1, 'abstract': 1, 'content': 1}.copy(), cluster=-1, pageRank=False):
		self.es.search(index=self.INDEX_NAME, doc_type=self.DOC_TYPE, body={
			"query": {
				"function_score": {
					"query": {
						"filtered": {
							"query_string": {
								"fields": ["content^" + str(field_weights['content']),
								           "title^" +
								           str(field_weights['title']),
								           "abstract^" + str(field_weights['abstract'])],
								"query": query
							}
						},
						"filter": ({"match": {"cluster": cluster}} if cluster != -1 else {"matchall": {}})
					},
					"boost": "1",
					"functions": [
						({
							 "field_value_factor": {
								 "field": "pagerank",
								 "factor": 5,
								 "missing": 1
							 },
							 "weight": 1
						 } if pageRank else {"weight": 1})
					],
					"score_mode": "multiply",
					"boost_mode": "multiply"
				}

			}
		})

#
# srcher = ElasticIndex()
# srcher.prompt()
# # prompt()
